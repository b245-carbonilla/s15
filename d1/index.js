console.log("Hello World!");

// Syntax of console.log: console.log(variable);
// comment - part of the code

/* 	
	2 Types of comments
	1. single line comment denoted by two slashes
	2. multi-line denoted by slash and asterisk

*/

// [Section] Syntax, statements
	// Statements in programming are instructions that we tell the computer to perform.

	// JS Statements usually end with semicolon(;)

	//	Semicolons are not required with in JS but we will use it to help us train to locate where statement ends.

	// A syntax in programming, it is the set of rules that we describes statemens must be constructed.

	// All lines/blocks of code should be written in specific matter to work. This is due to how this codes where initially programmed to function in a certain manner.

// [Section] Variable
	//Variables are used to contain data.

	//Any information that is used by an application is stored in what we call the memory.

	//When we create variables, certain portions of device memory is given a name that we call variables.

	//This makes it seasier for us to associate info stored in our devices to actual "names" information.

	//Declaring Variables
	//- it tells our devices that a variable name is created and is ready to store data.
		//Syntax
			/* let/const variableName; */

	let myVariable = "Ada Lovelace"; 
	let variable;
	//This will cause an undefined variable because the declared variable does not have initial value.
	console.log(variable);
	// Const keyword use when the value of variable won't change.

	const constVariable = "John Doe";
	//console.log() is useful for printing values of variables or certain results of code into the browser's console.
	console.log(myVariable);

	/*
		Guides in writing Variables
			1. Use the let keyword followed the variable name of your choose and use the assignment operator (=) to assign value.
			2. Variable names should start with lowercase character, use camelCasing for the multiple words.
			3. for const variables, use the 'const keyword'
				Note: If we use const keyword in declaring a variable, we cannot change value of it's variable.
			4. Variable name should be indicative(descriptive) of the value being stored to avoid confusion.
	*/

	// Declare an initialize
		// Initializing variables - the instance when a variable is given it's first/inital value.
		// Syntax:
			//let/const variableName = initial value;
		//Declaration and Initialization of the variable occur

		// var keyword = is hoisted, old from es6 update no longer used.
		//console.log(productName);

		let productName = "desktop computer";
		console.log(productName);

		//Declaration
		let desktopName; 
		console.log(desktopName);

		//Initialization of the value of variable desktopName
		desktopName = "Dell"
		console.log(desktopName);

		//Reassigning value
			// Syntax: variableName = newValue
		productName = "Personal Computer";
		console.log(productName);

		const name = "Chris";
		console.log(name);
		
		/*
		name = "Topher";
		console.log(name);
		*/

		//This will cause an error on our code because the productName variable is already taken.
		/*let productName = "Laptop"; */

		//var vs let/const
			//some of you may wonder why we used let and const keyword in declaring a variable when we search online, we usually see var.

		//var - is also used in declaring variable but var is an ecmaScript1 feature[(1997)].

		let lastName;
		lastName = "Mortel";
		console.log(lastName)

		/*
		Using var, bad practice.
		batch = "batch 241";
		

		var batch;
		console.log(batch); */

		//////////////////////////
//let const local/global scope
/*
		Scope essentially means where these variables are available for use.

		let/const are block scope.

		A block is a chunk of code bounded by {}.

*/

/*let outerVariable ="Hello";
{
	let innerVariable = "Hello Again";
	console.log(innerVariable);
}

console.log(outerVariable);*/


/*const outerVariable = "Hello";
{
	const innerVariable = "Hello Again";
}
console.log(outerVariable);
console.log(innerVariable);*/

var outerVariable = "Hello";
{
	var innerVariable = "Hello Again";
}
console.log(outerVariable);
console.log(innerVariable);

//Multiple variable declarations
let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

//Using a variable name with a reserve keyword
// const let = "Hello";
// console.log(let);

//[SECTION] Data Types
//Strings
/*
	Strings are a series of characters that create a word, a phrase, or a sentence or anything related to creating a text.
	Strings in JavaScript can be written using either single quote('') or double quuote ("").
*/

let country = "Philippines";
let province = 'Metro Manila';
console.log(country);
console.log(province);

//Concatenate(combine) strings
//Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province +", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

//Declaring a string using an escape character
let message = 'John\'s employees went home early.';
console.log(message);

message = "John's employees went home early.";
console.log(message);

// "\n" - referes to creating a new line in between text.
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

//Numbers
//Integers/Whole Number
let headCount = 27;
console.log(headCount);

//Decimal Number
let grade = 98.7;
console.log(grade);

//Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

//String
let planetDistance1 = "2e10";
console.log(planetDistance1);

//Combining text and strings
console.log("John's grade last quarter is " + grade);

//Boolean
//Boolean values are normall used to store values relating to the state of certain things 
//true or false

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

//Arrays
//Arrays are a special kind of data type that's use to store multiple value

/*
	Syntax:
	let/const arrayName = [elementA, elementB, elementC..]
*/
//similar data types
let grades = [98, 92.1, 90.1, 94.7];
console.log(grades);
//Array is a special kind of object
console.log(typeof grades);

//different data type
let details = ["John", 32, true];
console.log(details);


//Objects
//Objects are another special kind of 

/*
	Syntax: 
	let/const objectName = {
		propertyA: value ,
		propertyB: value
	}
*/

let person = {
	firstName: "John",
	lastName: "Smith",
	age: 32,
	isMarried: false,
	contact: ["+639655729788", "8123-4567"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}
console.log(person);

let person1 = {};
console.log(person1);

let myGrades = {
	firstGrading: 98,
	secondGrading: 92.1,
	thirdGrading: 90.1,
	fourthGrading: 94.7
}

/*
	Constant Objects and Arrays
	We can change the element of an arry to a constant variable.
*/
const anime = ["one piece", "one punch man", "your lie in April"];
console.log(anime);
//arrayName[indexNumber]
anime[0] = ['kimetsu no yaiba'];
console.log(anime);

const anime1 = ["one piece", "one punch man", "your lie in April"];
console.log(anime1);
/*anime1 = ['kimetsu no yaiba'];
console.log(anime1); */

//Null
//It is used to intentionally express the absence of a value in a variable/initialization
//intentionally walang value or 0
// si null is declared as null or wala tlagang value

let spouse = null;
console.log(spouse);

let myNumber = 0;
let myString = "";

//Undefined
//Represents the state of a variable that has been declared but without an assigned value. 
//pde pa magkaroon ng new value
let inARelationship;
console.log(inARelationship);
